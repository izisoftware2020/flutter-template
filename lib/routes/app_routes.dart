class AppRoutes {
  static const String SPLASH = '/';
  static const String AUTH = '/auth';
  static const String DASHBOARD = '/dashboard';
  static const String ACCOUNT = '/account';
  static const String POST = '/post';
}
