class Images {
  static const String splash_logo = 'assets/images/logo_splash.png';
  static const String logo_image = 'assets/images/logo.png';
  static const String no_internet = 'assets/images/opps_internet.png';
  static const String no_data = 'assets/images/no_data.png';
}
